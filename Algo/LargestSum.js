//Largest Sum of Consecutive Digits
//[1,2,3,4,3,5,4,6,7,8] =>
// Largest sum of consecutive digits
// num = 4
// Sum =>25

// Conditions
// num > array  ===> error message array exceed
// 10 - 4 + 1 = 7 // The loop will execute as:-
// total no of element - no consecutive pair + 1

function findLargest(array, num) {
  if (num > array) {
    throw new Error("Number not greater than array !");
  } else {
    let max = 0;
    for (let i = 0; i < array.length - num + 1; i++) {
      let temp = 0;
      for (let j = 0; j < num; j++) {
        console.log("i---->", i, "j------->", j);
        temp += array[i + j];
      }
      if (temp > max) {
        max = temp;
      }
    }
    return max;
  }
}

const result = findLargest([1, 2, 3, 4, 3, 5, 4, 6, 7, 8], 4);
console.log("Result-------->", result);

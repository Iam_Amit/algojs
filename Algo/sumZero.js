// checking sum zero -pattern
//[-5,-4,-3,-2,0,2,4,6,8]  -> Input

// function sumZero(array) {
//   for (let num of array) {
//     console.log("outer loop", num);
//     for (let j = 1; j < array.length; j++) {
//       console.log("Inner loop");
//       if (num + array[j] === 0) {
//         return [num, array[j]];
//       }
//     }
//   }
// }

// const abc = sumZero([-5, -4, -3, -2, 0, 2, 4, 6, 8]);

// console.log("abc=----------->", abc);

// for the above program the time complexity will be quadratic that is o(N^2)

//------------OPTIMIZED VERSION OF THE SUMZERO-----------------//
//[-5, -4, -3, -2, 0, 2, 4, 6, 8]
function sumZero(array) {
  let left = 0;
  let right = array.length - 1;

  while (left < right) {
    sum = array[left] + array[right];
    console.log("sum--------->", sum);
    if (sum === 0) {
      return [array[left], array[right]];
    } else if (sum > 0) {
      console.log("inside If--------->", sum);
      right--;
    } else {
      console.log("inside else--------->");
      left++;
    }
  }
}

const abc = sumZero([-5, -4, -3, -2, 0, 2, 4, 6, 8]);

console.log("sumZero--------->", abc);

// above mention code the time complexity is linear
// If the sum(+ve) is greater than 0 then we need decrement the right variable or pointer
// If the sum(-ve) is less than 0 then we need to increment the left variable or pointer

//Counting Unique Numbers Problem
// WAP to find the unique number in the array
// [1,1,2,2,3,4,4,5,6,7,8,8]
// [1,2,3]=> 3
// output => 8

// for the timeComplexity of o(n^2) quadratic

function countUnique(array) {
  let temp = [];
  for (let i = 0; i < array.length; i++) {
    console.log("array[i]==========>", array[i]);
    if (temp.length) {
      let eleExist = false;
      for (let j = 0; j < temp.length; j++) {
        console.log("j------->");
        if (array[i] == temp[j]) eleExist = true;
      }
      if (!eleExist) {
        temp.push(array[i]);
      }
    } else {
      temp.push(array[i]);
    }
  }
  return temp;
}

const result = countUnique([7, 1, 2, 2, 3, 4, 4, 5, 6, 7, 8, 8]);

console.log("result-------->", result);
//||************* for timeComplexity o(n)*****************************||

/**conditions
 * i=0 j=1
 * if both i and j are eqaual then we will increment J++
 *
 * and if array[i]!== array[j] then
 * 1. i++
 * 2. array[j]= array[i]
 *
 *
 */

// function countUnique(array) {
//   if (array.length > 0) {
//     let i = 0;
//     for (let j = 1; j < array.length; j++) {
//       if (array[i] !== array[j]) {
//         i++;
//         array[i] = array[j];
//       }
//     }
//     return i + 1; //as our index is starting from 0 is the reason we increment it by 1
//   } else throw new Error("Array is empty!");
// }

// const result = countUnique([1, 1, 2, 2, 3, 4, 4, 5, 6, 7, 8, 8]);

// console.log("result-------->", result);

/**
 * For the above mention program we have to calculate the unique element from the array so for that we will follow the following steps as:-
 * 1.  we will check if the array is empty or not if empty then return empty array  error
 * 2. Else if the array is not empty then we will initialize the array from i=0 and j=1
 * 3. Then we will loop the j from index 1 to the array length And in the loop we will check the condition
 * 4. If the value of i is not equal to alue of j array[i] !== array[j] then we will increment the i by 1 and put the value of j in i
 * 5. Since the index is started from 0 so we will increment the i by 1 so to give the unique number of element in array.
 *
 */

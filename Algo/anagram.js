// String anagram
// 'hello' ===>'elloh'

// condition
//length check(for both string)
//{h:0,e:0,l:0:o:0}

function isAnagram(str1, str2) {
  if (str1.length !== str2.length) {
    return false;
  }

  let counter = {};
  for (let letters of str1) {
    //   for (let letters = 0; letters < str1.length; letters++) {
    // it will display the undefined so to remove this in js we can do it via OR(REACTJS)
    counter[letters] = (counter[letters] || 0) + 1;
    console.log("counter[letters]------->", counter[letters]);
  }

  console.log(counter);

  for (let items of str2) {
    if (!counter[items]) {
      console.log("items--------->", items);
      return false;
    }
    counter[items] -= 1;
  }
  return true;
}

const check = isAnagram("qwerty", "ytrewq");
console.log(check);

// for....of loop=====> returns the array of [key, value] for each iteration

//
